# Changelog

All notable changes to `ccc-api` will be documented in this file.

## 1.1.0 - 2022-02-15

### Added

- Recording download function

## 1.0.0 - 2021-03-16

- initial release

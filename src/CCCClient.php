<?php

namespace Layer7\CCCApi;

use stdClass;

class CCCClient
{
	private $username;
	private $password;
	private $client_id;
	private $passphrase;

	private $base_url = 'https://api.ccc.uno/ws/';

	/**
	 * __construct
	 */
	public function __construct($username, $client_id, $password, $passphrase = '')
	{
		$this->username = $username;
		$this->password = $password;
		$this->client_id = $client_id;
		$this->passphrase = $passphrase;
	}

	/**
	 * Ping
	 */
	public function Ping()
	{
		$url = $this->base_url.'Ping';

		$Data = new stdClass();

		$Data->ping = "ping";

		return $this->Request($url, $Data);
	}

	/**
	 * BlackListStore
	 */
	public function BlackListStore($phone_number, $common_key = null, $ban_hours = null, $ban_until = null)
	{
		$url = $this->base_url.'BlackList';

		$Data = new stdClass();

		$Data->phone_number = $phone_number;
		$Data->common_key = $common_key;
		$Data->ban_hours = $ban_hours;
		$Data->ban_until = $ban_until;

		return $this->Request($url, $Data);
	}

	/**
	 * BlackListEnable
	 */
	public function BlackListEnable($phone_number, $active = 'true', $common_key = null)
	{
		$url = $this->base_url.'BlackList/Enable';

		$Data = new stdClass();

		$Data->phone_number = $phone_number;
		$Data->common_key = $common_key;
		$Data->active = $active;

		return $this->Request($url, $Data);
	}

	/**
	 * BlackListShow
	 */
	public function BlackListShow($rule_id)
	{
		$url = $this->base_url.'BlackList/'.$rule_id;

		$Data = new stdClass();

		return $this->Request($url, $Data);
	}

	/**
	 * BlackListDelete
	 */
	public function BlackListDelete($rule_id)
	{
		$url = $this->base_url.'BlackList/'.$rule_id.'/Delete';

		$Data = new stdClass();

		return $this->Request($url, $Data);
	}

	/**
	 * LeadStore
	 */
	public function LeadStore($campaign_id = null, $leads = [])
	{
		$url = $this->base_url.'Lead';

		$Data = new stdClass();

		$Data->campaign_id = $campaign_id;
		$Data->Leads = $leads;

		return $this->Request($url, $Data);
	}

	/**
	 * CallHistorySearch
	 */
	public function CallHistorySearch($datetime_from, $datetime_to, $page = 1, $columns = '', $options = ['list_columns', 'dispositions'], $filters = [])
	{
		$url = $this->base_url.'CallHistory/Search';

		$Data = new stdClass();

		$Data->datetime_from = $datetime_from;
		$Data->datetime_to = $datetime_to;
		$Data->page = $page;
		$Data->columns = $columns;
		$Data->options = $options;
		$Data->filters = $filters;

		return $this->Request($url, $Data);
	}

	/**
	 * CallHistorySearchByID
	 */
	public function CallHistorySearchByID($call_id)
	{
		return $this->CallHistorySearch(null, null, 1, '', ['list_columns', 'dispositions'], ['call_id' => $call_id]);
	}

	/**
	 * SMSHistorySearch
	 */
	public function SMSHistorySearch($datetime_from, $datetime_to, $page = 1, $pagesize = 10, $columns = '', $options = [], $filters = [])
	{
		$url = $this->base_url.'SMS/History/Search';

		$Data = new stdClass();

		$Data->datetime_from = $datetime_from;
		$Data->datetime_to = $datetime_to;
		$Data->page = $page;
		$Data->pagesize = $pagesize;
		$Data->columns = $columns;
		$Data->options = $options;
		$Data->filters = $filters;

		return $this->Request($url, $Data);
	}

	/**
	 * AgentGetStatus
	 */
	public function AgentGetStatus($user_id)
	{
		$url = $this->base_url.'Agent/getStatus';

		$Data = new stdClass();
		$Data->user_id = $user_id;

		return $this->Request($url, $Data);
	}

	/**
	 * AgentGetTimes
	 */
	public function AgentGetTimes($datetime_from, $datetime_to, $type = '', $filters = [])
	{
		$url = $this->base_url.'Agent/getTimes';

		$Data = new stdClass();
		$Data->datetime_from = $datetime_from;
		$Data->datetime_to = $datetime_to;
		$Data->type = $type;
		$Data->options = $filters;

		return $this->Request($url, $Data);
	}

	/**
	 * SMSSend
	 */
	public function SMSSend($number_to, $text, $schedule_time = null)
	{
		$url = $this->base_url.'SMS/Send';

		$Data = new stdClass();
		$Data->number_to = $number_to;
		$Data->text = $text;
		$Data->schedule_time = $schedule_time;

		return $this->Request($url, $Data);
	}

	/**
	 * RecordingDownload
	 */
	public function RecordingDownload($call_id)
	{
		$url = $this->base_url.'Recording';

		$Data = new stdClass();
		$Data->call_id = $call_id;

		return $this->Request($url, $Data);
	}

	/**
	 * SurveyLog
	 */
	public function SurveyLog($datetime_from, $datetime_to, $instance_id, $page = 1, $pagesize = 1000)
	{
		$url = $this->base_url.'SurveyLog';

		$Data = new stdClass();
		$Data->datetime_from = $datetime_from;
		$Data->datetime_to = $datetime_to;
		$Data->instance_id = $instance_id;
		$Data->page = $page;
		$Data->pagesize = $pagesize;

		return $this->Request($url, $Data);
	}
	
	/**
	 * InboundSummaryByDAy 
	 */
	public function InboundSummaryByDay($datetime_from, $datetime_to)
	{
		$url = $this->base_url.'InboundSummaryByDay';

		$Data = new stdClass();
		$Data->datetime_from = $datetime_from;
		$Data->datetime_to = $datetime_to;

		return $this->Request($url, $Data);
	}

	/**
	 * Request
	 */
	private function Request($URL, $Data)
	{
		if (!empty($this->passphrase))
			$Data->passphrase = $this->passphrase;
			
		$request = $this->getRequestString($this->username, $this->client_id, $this->password, $Data);

		$curl = curl_init();

		curl_setopt_array($curl, [
			CURLOPT_URL => $URL,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 900,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $request,
		]);

		$response = curl_exec($curl);
		$err = curl_error($curl);

		$res = new stdClass();
		
		$res->request_url = $URL;
		$res->request = $request;
		$res->error = 0;
		$res->http_response_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		
		curl_close($curl);

		if (!empty($err))
		{
			$res->error = 1;
			$res->error_text = $err;
			return $res;
		}

		if ($response == "Unauthorized")
		{
			$res->error = 401;
			$res->error_text = $response;
			return $res;
		}

		$res->response = $response;

		return $res;
	}

	/**
	 * getRequestString
	 */
	private function getRequestString($authUsername, $client_id, $password, $Data)
	{
		$requestDateTime = date('Y-m-d H:i:s');
		$requestToken = $this->getRequestToken($authUsername, $client_id, $password, $Data, $requestDateTime);
		$requestData = json_encode($Data);

		$request =
			"AuthUsername = {$authUsername}\n".
			"AuthToken = {$requestToken}\n".
			"RequestDateTime = {$requestDateTime}\n".
			"Data = {$requestData}";

		return $request;
	}

	/**
	 * getRequestToken
	 */
	private function getRequestToken($authUsername, $client_id, $password, $Data, $requestDateTime)
	{
		$password = hash('sha256', $password);
		$token1 = hash('sha256', json_encode($Data));
		$token2 = hash('sha256', "{$client_id}:{$authUsername}:{$password}");
		$token3 = hash('sha256', "{$token1}:{$token2}:{$requestDateTime}");

		return $token3;
	}
}

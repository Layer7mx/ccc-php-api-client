<?php

require_once 'vendor/autoload.php';
require_once 'credentials.php';

/*
     Usage: php inbound_summary_by_day.php
*/

use Layer7\CCCApi\CCCClient;

$Client = new CCCClient($username, $client_id, $password, $passphrase);

// Dates to fetch from API
$date_from = '2022-03-31 00:00:00';
$date_to = '2022-04-02 23:59:59';
$Response = $Client->InboundSummaryByDay($date_from, $date_to);

print_r(json_decode($Response->response));
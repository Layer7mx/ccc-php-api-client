<?php

require_once 'vendor/autoload.php';
require_once 'credentials.php';

/*
    Usage: php recording.php CALLID
    Example: php recording.php 1337
*/

use Layer7\CCCApi\CCCClient;

$Client = new CCCClient($username, $client_id, $password, $passphrase);
$call_id = $argv[1];

$ResponseRaw = $Client->RecordingDownload($call_id);

if ($ResponseRaw->http_response_code != '200')
{
    print_r($ResponseRaw->response .  "\n");
    die;
}

// convert response data to stdclass
$Response = json_decode($ResponseRaw->response);

// create folder if not exists already
if (!file_exists($call_id))
{
    mkdir($call_id);
}

// loop through recordings to save them in a common folder.
foreach ($Response->data->recordings as $Recording)
{
    // decode file contents
    $recording = base64_decode($Recording->audio);
    // save into a file
    $filename = "{$call_id}/{$Recording->filename}";
    file_put_contents($filename, $recording);
    echo "Saved file: {$filename}\n";
}